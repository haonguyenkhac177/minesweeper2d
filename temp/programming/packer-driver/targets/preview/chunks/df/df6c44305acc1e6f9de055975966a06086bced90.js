System.register(["__unresolved_0", "cc", "moment"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, __checkObsolete__, __checkObsoleteInNamespace__, _decorator, Component, Node, game, Prefab, instantiate, Layout, Size, UITransform, Label, macro, AudioSource, moment, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _crd, ccclass, property, BlockType, GameState, GameManager;

  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

  function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'transform-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfmoment(extras) {
    _reporterNs.report("moment", "moment", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      __checkObsolete__ = _cc.__checkObsolete__;
      __checkObsoleteInNamespace__ = _cc.__checkObsoleteInNamespace__;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      game = _cc.game;
      Prefab = _cc.Prefab;
      instantiate = _cc.instantiate;
      Layout = _cc.Layout;
      Size = _cc.Size;
      UITransform = _cc.UITransform;
      Label = _cc.Label;
      macro = _cc.macro;
      AudioSource = _cc.AudioSource;
    }, function (_moment) {
      moment = _moment.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "70dbdkS/wlKAYEwiB4I3JUl", "GameManager", undefined);

      __checkObsolete__(['_decorator', 'Component', 'Node', 'game', 'Prefab', 'instantiate', 'Layout', 'Size', 'UITransform', 'Widget', 'Animation', 'cclegacy', 'Label', 'macro', 'AudioSource']);

      ({
        ccclass,
        property
      } = _decorator);

      BlockType = /*#__PURE__*/function (BlockType) {
        BlockType[BlockType["Box1"] = 0] = "Box1";
        BlockType[BlockType["Box2"] = 1] = "Box2";
        return BlockType;
      }(BlockType || {});

      ;

      GameState = /*#__PURE__*/function (GameState) {
        GameState[GameState["GS_INIT"] = 0] = "GS_INIT";
        GameState[GameState["GS_PLAYING"] = 1] = "GS_PLAYING";
        GameState[GameState["GS_END"] = 2] = "GS_END";
        return GameState;
      }(GameState || {});

      ; // const moment = require('moment');
      // export const BLOCK_SIZE = 30;

      _export("GameManager", GameManager = (_dec = ccclass('GameManager'), _dec2 = property({
        type: Prefab
      }), _dec3 = property({
        type: Prefab
      }), _dec4 = property({
        type: Prefab
      }), _dec5 = property({
        type: Node
      }), _dec6 = property({
        type: Node
      }), _dec7 = property({
        type: Node
      }), _dec8 = property({
        type: Node
      }), _dec9 = property({
        type: Node
      }), _dec10 = property({
        type: AudioSource
      }), _dec(_class = (_class2 = class GameManager extends Component {
        constructor() {
          var _this;

          super(...arguments);
          _this = this;

          _initializerDefineProperty(this, "boxPrefab", _descriptor, this);

          _initializerDefineProperty(this, "bomPrefab", _descriptor2, this);

          _initializerDefineProperty(this, "boxIndexPrefab", _descriptor3, this);

          _initializerDefineProperty(this, "startMenu", _descriptor4, this);

          _initializerDefineProperty(this, "gameTitle", _descriptor5, this);

          _initializerDefineProperty(this, "settingMenu", _descriptor6, this);

          _initializerDefineProperty(this, "topMenu", _descriptor7, this);

          _initializerDefineProperty(this, "mapLayout", _descriptor8, this);

          _initializerDefineProperty(this, "clickSound", _descriptor9, this);

          this.btnDisabled = false;
          this.minefield = [];
          this.startTime = 0;
          this.isRunning = false;
          this.init = /*#__PURE__*/_asyncToGenerator(function* () {
            _this.clickSound.play();

            _this.startMenu.active = true;
            _this.settingMenu.active = true;
            _this.gameTitle.active = true;
            _this.topMenu.active = false; // await this.genBox()
            // this.mapLayout.children.forEach((i,index)=>{
            //     setTimeout(() => {
            //         i.getComponent(Animation).play('showBoxAnimation')
            //     },index*50 );
            // })
            // console.log(this.btnDisabled)
          });

          this.playing = () => {
            this.startMenu.active = false;
            this.settingMenu.active = false;
            this.gameTitle.active = false;
            this.topMenu.active = true;
            this.genBox();
          };

          this.genBox = /*#__PURE__*/_asyncToGenerator(function* () {
            _this.btnDisabled = true;

            _this.mapLayout.removeAllChildren();

            var newLayout = _this.mapLayout.getComponent(UITransform);

            var BLOCK_SIZE = instantiate(_this.boxPrefab).getComponent(UITransform).width;
            var sizeBoxNumber = 9;
            var numBombs = 9;
            _this.minefield = Array(sizeBoxNumber * sizeBoxNumber).fill(0);

            for (var i = 0; i < numBombs; i++) {
              var bombIndex = void 0;

              do {
                // Chọn một ô ngẫu nhiên để đặt bom
                bombIndex = Math.floor(Math.random() * _this.minefield.length);
              } while (_this.minefield[bombIndex] === -1); // Đặt bom vào ô đã chọn


              _this.minefield[bombIndex] = -1; // Tăng giá trị các ô xung quanh bom

              _this.increaseNeighbors(bombIndex, sizeBoxNumber);
            }

            var spacingX = 3;
            var LayoutSize = sizeBoxNumber * (BLOCK_SIZE + spacingX) - spacingX;
            var totalBlock = sizeBoxNumber * sizeBoxNumber;
            _this.mapLayout.getComponent(Layout).cellSize = new Size(BLOCK_SIZE, BLOCK_SIZE);
            newLayout.setContentSize(LayoutSize, LayoutSize);

            for (var j = 0; j < totalBlock; j++) {
              switch (_this.minefield[j]) {
                case -1:
                  var newBom = instantiate(_this.bomPrefab);
                  newBom.active = false; // setTimeout(() => {

                  _this.mapLayout.addChild(newBom); // }, j * 50)


                  break;

                case 0:
                  var newBox = instantiate(_this.boxPrefab);
                  newBox.active = false; // setTimeout(() => {

                  _this.mapLayout.addChild(newBox); // }, j * 50)


                  break;

                default:
                  var newIndex = instantiate(_this.boxIndexPrefab);
                  newIndex.getComponentInChildren(Label).string = _this.minefield[j].toString();
                  newIndex.active = false; // setTimeout(() => {

                  _this.mapLayout.addChild(newIndex); // }, j * 50)


                  break;
              }
            }

            var list = _this.mapLayout.children;
            list.forEach((i, index) => {
              setTimeout(() => {
                i.active = true;
              }, index * 5);
            });
            _this.btnDisabled = false;
          });

          this.increaseNeighbors = (index, gridSize) => {
            var neighbors = this.getNeighborIndices(index, gridSize);

            for (var neighbor of neighbors) {
              if (this.minefield[neighbor] !== -1) {
                this.minefield[neighbor]++;
              }
            }
          };

          this.getNeighborIndices = (index, gridSize) => {
            var row = Math.floor(index / gridSize);
            var col = index % gridSize;
            var neighbors = [];

            for (var i = -1; i <= 1; i++) {
              for (var j = -1; j <= 1; j++) {
                var newRow = row + i;
                var newCol = col + j; // Kiểm tra xem ô mới có nằm trong biên không

                if (newRow >= 0 && newRow < gridSize && newCol >= 0 && newCol < gridSize) {
                  neighbors.push(newRow * gridSize + newCol);
                }
              }
            }

            return neighbors;
          };

          this.onResetButtonClicked = /*#__PURE__*/_asyncToGenerator(function* () {
            // this.stopTime()
            _this.startTimer(); // if (this.isRunning) {
            //     this.stopTime();
            // } else {
            //     this.startTimer();
            // }


            yield _this.mapLayout.removeAllChildren();
            yield _this.genBox();
          });
        }

        setCurState(value) {
          switch (value) {
            case GameState.GS_INIT:
              this.init();
              break;

            case GameState.GS_PLAYING:
              this.playing();
              break;

            case GameState.GS_END:
              break;
          }
        }

        onPlayButtonClicked() {
          this.setCurState(GameState.GS_PLAYING); // this.startMenu.active = false
          // this.settingMenu.active = false

          this.startTime = (_crd && moment === void 0 ? (_reportPossibleCrUseOfmoment({
            error: Error()
          }), moment) : moment)().unix(); // this.schedule(this.countTime, 1, macro.REPEAT_FOREVER);

          this.startTimer();
        }

        countTime() {
          var currentTime = (_crd && moment === void 0 ? (_reportPossibleCrUseOfmoment({
            error: Error()
          }), moment) : moment)().unix();
          var elapsedSeconds = currentTime - this.startTime;
          var formattedTime = (_crd && moment === void 0 ? (_reportPossibleCrUseOfmoment({
            error: Error()
          }), moment) : moment).utc(elapsedSeconds * 1000).format("HH:mm:ss");
          this.topMenu.getChildByName('Time').getComponent(Label).string = formattedTime;
          console.log(formattedTime);
        }

        startTimer() {
          this.topMenu.getChildByName('Time').getComponent(Label).string = '00:00:00';
          this.isRunning = true;
          this.startTime = (_crd && moment === void 0 ? (_reportPossibleCrUseOfmoment({
            error: Error()
          }), moment) : moment)().unix(); // Gọi hàm updateTimer mỗi giây

          this.schedule(this.countTime, 1, macro.REPEAT_FOREVER);
        }

        stopTime() {
          this.isRunning = false;
          this.unschedule(this.countTime);
        }

        onSettingButtonClicked() {
          this.startMenu.active = false;
          this.settingMenu.active = true;
        }

        onBackButtonClicked() {
          this.startMenu.active = true;
          this.settingMenu.active = false;
        }

        onExitButtonClicked() {
          window.location.href = 'about:blank';
          game.end();
        }

        start() {
          // this.settingMenu.active = false
          this.setCurState(GameState.GS_INIT);
        }

        update(deltaTime) {}

      }, (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "boxPrefab", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "bomPrefab", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "boxIndexPrefab", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "startMenu", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "gameTitle", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "settingMenu", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "topMenu", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "mapLayout", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "clickSound", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=df6c44305acc1e6f9de055975966a06086bced90.js.map