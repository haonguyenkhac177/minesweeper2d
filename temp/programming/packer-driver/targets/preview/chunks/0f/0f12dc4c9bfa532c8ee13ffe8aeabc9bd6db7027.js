System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, __checkObsolete__, __checkObsoleteInNamespace__, _decorator, Component, Node, game, Prefab, instantiate, Layout, Size, UITransform, Animation, Label, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _crd, ccclass, property, BlockType, GameState, GameManager;

  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

  function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'transform-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      __checkObsolete__ = _cc.__checkObsolete__;
      __checkObsoleteInNamespace__ = _cc.__checkObsoleteInNamespace__;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      game = _cc.game;
      Prefab = _cc.Prefab;
      instantiate = _cc.instantiate;
      Layout = _cc.Layout;
      Size = _cc.Size;
      UITransform = _cc.UITransform;
      Animation = _cc.Animation;
      Label = _cc.Label;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "70dbdkS/wlKAYEwiB4I3JUl", "GameManager", undefined);

      __checkObsolete__(['_decorator', 'Component', 'Node', 'game', 'Prefab', 'instantiate', 'Layout', 'Size', 'UITransform', 'Widget', 'Animation', 'cclegacy', 'Label']);

      ({
        ccclass,
        property
      } = _decorator);

      BlockType = /*#__PURE__*/function (BlockType) {
        BlockType[BlockType["Box1"] = 0] = "Box1";
        BlockType[BlockType["Box2"] = 1] = "Box2";
        return BlockType;
      }(BlockType || {});

      ;

      GameState = /*#__PURE__*/function (GameState) {
        GameState[GameState["GS_INIT"] = 0] = "GS_INIT";
        GameState[GameState["GS_PLAYING"] = 1] = "GS_PLAYING";
        GameState[GameState["GS_END"] = 2] = "GS_END";
        return GameState;
      }(GameState || {});

      ; // export const BLOCK_SIZE = 30;

      _export("GameManager", GameManager = (_dec = ccclass('GameManager'), _dec2 = property({
        type: Prefab
      }), _dec3 = property({
        type: Prefab
      }), _dec4 = property({
        type: Prefab
      }), _dec5 = property({
        type: Node
      }), _dec6 = property({
        type: Node
      }), _dec7 = property({
        type: Node
      }), _dec(_class = (_class2 = class GameManager extends Component {
        constructor() {
          var _this;

          super(...arguments);
          _this = this;

          _initializerDefineProperty(this, "boxPrefab", _descriptor, this);

          _initializerDefineProperty(this, "bomPrefab", _descriptor2, this);

          _initializerDefineProperty(this, "boxIndexPrefab", _descriptor3, this);

          _initializerDefineProperty(this, "startMenu", _descriptor4, this);

          _initializerDefineProperty(this, "settingMenu", _descriptor5, this);

          _initializerDefineProperty(this, "mapLayout", _descriptor6, this);

          this.minefield = [];
          this.init = /*#__PURE__*/_asyncToGenerator(function* () {
            yield _this.genBox();
            console.log(_this.mapLayout.children);

            _this.mapLayout.children.forEach(i => {
              i.getComponent(Animation).play('showBoxAnimation');
            });
          });
          this.genBox = /*#__PURE__*/_asyncToGenerator(function* () {
            var newLayout = _this.mapLayout.getComponent(UITransform);

            var BLOCK_SIZE = instantiate(_this.boxPrefab).getComponent(UITransform).width;
            var sizeBoxNumber = 9;
            var numBombs = 9;
            _this.minefield = Array(sizeBoxNumber * sizeBoxNumber).fill(0);

            for (var i = 0; i < numBombs; i++) {
              var bombIndex = void 0;

              do {
                // Chọn một ô ngẫu nhiên để đặt bom
                bombIndex = Math.floor(Math.random() * _this.minefield.length);
              } while (_this.minefield[bombIndex] === -1); // Đặt bom vào ô đã chọn


              _this.minefield[bombIndex] = -1; // Tăng giá trị các ô xung quanh bom

              _this.increaseNeighbors(bombIndex, sizeBoxNumber);
            }

            console.log('aaa', _this.minefield);
            var spacingX = 3;
            var LayoutSize = sizeBoxNumber * (BLOCK_SIZE + spacingX) - spacingX;
            var totalBlock = sizeBoxNumber * sizeBoxNumber;
            _this.mapLayout.getComponent(Layout).cellSize = new Size(BLOCK_SIZE, BLOCK_SIZE);
            newLayout.setContentSize(LayoutSize, LayoutSize);

            for (var j = 0; j < totalBlock; j++) {
              switch (_this.minefield[j]) {
                case -1:
                  _this.mapLayout.addChild(instantiate(_this.bomPrefab));

                  break;

                case 0:
                  _this.mapLayout.addChild(instantiate(_this.boxPrefab));

                  break;

                default:
                  var newBoxNode = instantiate(_this.boxIndexPrefab);
                  newBoxNode.getComponentInChildren(Label).string = _this.minefield[j].toString();

                  _this.mapLayout.addChild(newBoxNode);

                  break;
              } // if(this.minefield[j]===-1){
              //     this.mapLayout.addChild(instantiate(this.bomPrefab))
              // }else{
              //     this.mapLayout.addChild(instantiate(this.boxPrefab))
              // }

            }
          });

          this.increaseNeighbors = (index, gridSize) => {
            var neighbors = this.getNeighborIndices(index, gridSize);

            for (var neighbor of neighbors) {
              if (this.minefield[neighbor] !== -1) {
                this.minefield[neighbor]++;
              }
            }
          };

          this.getNeighborIndices = (index, gridSize) => {
            var row = Math.floor(index / gridSize);
            var col = index % gridSize;
            var neighbors = [];

            for (var i = -1; i <= 1; i++) {
              for (var j = -1; j <= 1; j++) {
                var newRow = row + i;
                var newCol = col + j; // Kiểm tra xem ô mới có nằm trong biên không

                if (newRow >= 0 && newRow < gridSize && newCol >= 0 && newCol < gridSize) {
                  neighbors.push(newRow * gridSize + newCol);
                }
              }
            }

            return neighbors;
          };
        }

        setCurState(value) {
          switch (value) {
            case GameState.GS_INIT:
              this.init();
              break;

            case GameState.GS_PLAYING:
              break;

            case GameState.GS_END:
              break;
          }
        }

        onPlayButtonClicked() {
          this.startMenu.active = false;
          this.settingMenu.active = false;
        }

        onSettingButtonClicked() {
          this.startMenu.active = false;
          this.settingMenu.active = true;
        }

        onBackButtonClicked() {
          this.startMenu.active = true;
          this.settingMenu.active = false;
        }

        onExitButtonClicked() {
          window.location.href = 'about:blank';
          game.end();
        }

        onResetButtonClicked() {
          console.log('reset');
        }

        start() {
          this.settingMenu.active = false;
          this.setCurState(GameState.GS_INIT);
        }

        update(deltaTime) {}

      }, (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "boxPrefab", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "bomPrefab", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "boxIndexPrefab", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "startMenu", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "settingMenu", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "mapLayout", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=0f12dc4c9bfa532c8ee13ffe8aeabc9bd6db7027.js.map