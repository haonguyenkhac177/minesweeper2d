System.register(["__unresolved_0", "cc", "moment"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, __checkObsolete__, __checkObsoleteInNamespace__, _decorator, Component, Node, game, Prefab, instantiate, Layout, Size, UITransform, Label, macro, AudioClip, moment, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _crd, ccclass, property, BlockType, GameState, GameManager;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'transform-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfmoment(extras) {
    _reporterNs.report("moment", "moment", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      __checkObsolete__ = _cc.__checkObsolete__;
      __checkObsoleteInNamespace__ = _cc.__checkObsoleteInNamespace__;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      game = _cc.game;
      Prefab = _cc.Prefab;
      instantiate = _cc.instantiate;
      Layout = _cc.Layout;
      Size = _cc.Size;
      UITransform = _cc.UITransform;
      Label = _cc.Label;
      macro = _cc.macro;
      AudioClip = _cc.AudioClip;
    }, function (_moment) {
      moment = _moment.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "70dbdkS/wlKAYEwiB4I3JUl", "GameManager", undefined);

      __checkObsolete__(['_decorator', 'Component', 'Node', 'game', 'Prefab', 'instantiate', 'Layout', 'Size', 'UITransform', 'Widget', 'Animation', 'cclegacy', 'Label', 'macro', 'AudioClip']);

      ({
        ccclass,
        property
      } = _decorator);

      BlockType = /*#__PURE__*/function (BlockType) {
        BlockType[BlockType["Box1"] = 0] = "Box1";
        BlockType[BlockType["Box2"] = 1] = "Box2";
        return BlockType;
      }(BlockType || {});

      ;

      GameState = /*#__PURE__*/function (GameState) {
        GameState[GameState["GS_INIT"] = 0] = "GS_INIT";
        GameState[GameState["GS_PLAYING"] = 1] = "GS_PLAYING";
        GameState[GameState["GS_END"] = 2] = "GS_END";
        return GameState;
      }(GameState || {});

      ; // const moment = require('moment');
      // export const BLOCK_SIZE = 30;

      _export("GameManager", GameManager = (_dec = ccclass('GameManager'), _dec2 = property({
        type: Prefab
      }), _dec3 = property({
        type: Prefab
      }), _dec4 = property({
        type: Prefab
      }), _dec5 = property({
        type: Node
      }), _dec6 = property({
        type: Node
      }), _dec7 = property({
        type: Node
      }), _dec8 = property({
        type: Node
      }), _dec9 = property({
        type: Node
      }), _dec10 = property({
        type: AudioClip
      }), _dec(_class = (_class2 = class GameManager extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "boxPrefab", _descriptor, this);

          _initializerDefineProperty(this, "bomPrefab", _descriptor2, this);

          _initializerDefineProperty(this, "boxIndexPrefab", _descriptor3, this);

          _initializerDefineProperty(this, "startMenu", _descriptor4, this);

          _initializerDefineProperty(this, "gameTitle", _descriptor5, this);

          _initializerDefineProperty(this, "settingMenu", _descriptor6, this);

          _initializerDefineProperty(this, "topMenu", _descriptor7, this);

          _initializerDefineProperty(this, "mapLayout", _descriptor8, this);

          _initializerDefineProperty(this, "clickSound", _descriptor9, this);

          this.btnDisabled = false;
          this.minefield = [];
          this.startTime = 0;
          this.isRunning = false;

          this.init = async () => {
            this.startMenu.active = true;
            this.settingMenu.active = true;
            this.gameTitle.active = true;
            this.topMenu.active = false; // await this.genBox()
            // this.mapLayout.children.forEach((i,index)=>{
            //     setTimeout(() => {
            //         i.getComponent(Animation).play('showBoxAnimation')
            //     },index*50 );
            // })
            // console.log(this.btnDisabled)
          };

          this.playing = () => {
            this.startMenu.active = false;
            this.settingMenu.active = false;
            this.gameTitle.active = false;
            this.topMenu.active = true;
            this.genBox();
          };

          this.genBox = async () => {
            this.btnDisabled = true;
            this.mapLayout.removeAllChildren();
            const newLayout = this.mapLayout.getComponent(UITransform);
            const BLOCK_SIZE = instantiate(this.boxPrefab).getComponent(UITransform).width;
            const sizeBoxNumber = 9;
            const numBombs = 9;
            this.minefield = Array(sizeBoxNumber * sizeBoxNumber).fill(0);

            for (let i = 0; i < numBombs; i++) {
              let bombIndex;

              do {
                // Chọn một ô ngẫu nhiên để đặt bom
                bombIndex = Math.floor(Math.random() * this.minefield.length);
              } while (this.minefield[bombIndex] === -1); // Đặt bom vào ô đã chọn


              this.minefield[bombIndex] = -1; // Tăng giá trị các ô xung quanh bom

              this.increaseNeighbors(bombIndex, sizeBoxNumber);
            }

            const spacingX = 3;
            const LayoutSize = sizeBoxNumber * (BLOCK_SIZE + spacingX) - spacingX;
            const totalBlock = sizeBoxNumber * sizeBoxNumber;
            this.mapLayout.getComponent(Layout).cellSize = new Size(BLOCK_SIZE, BLOCK_SIZE);
            newLayout.setContentSize(LayoutSize, LayoutSize);

            for (let j = 0; j < totalBlock; j++) {
              switch (this.minefield[j]) {
                case -1:
                  const newBom = instantiate(this.bomPrefab);
                  newBom.active = false; // setTimeout(() => {

                  this.mapLayout.addChild(newBom); // }, j * 50)

                  break;

                case 0:
                  const newBox = instantiate(this.boxPrefab);
                  newBox.active = false; // setTimeout(() => {

                  this.mapLayout.addChild(newBox); // }, j * 50)

                  break;

                default:
                  const newIndex = instantiate(this.boxIndexPrefab);
                  newIndex.getComponentInChildren(Label).string = this.minefield[j].toString();
                  newIndex.active = false; // setTimeout(() => {

                  this.mapLayout.addChild(newIndex); // }, j * 50)

                  break;
              }
            }

            const list = this.mapLayout.children;
            list.forEach((i, index) => {
              setTimeout(() => {
                i.active = true;
              }, index * 5);
            });
            this.btnDisabled = false;
          };

          this.increaseNeighbors = (index, gridSize) => {
            const neighbors = this.getNeighborIndices(index, gridSize);

            for (const neighbor of neighbors) {
              if (this.minefield[neighbor] !== -1) {
                this.minefield[neighbor]++;
              }
            }
          };

          this.getNeighborIndices = (index, gridSize) => {
            const row = Math.floor(index / gridSize);
            const col = index % gridSize;
            const neighbors = [];

            for (let i = -1; i <= 1; i++) {
              for (let j = -1; j <= 1; j++) {
                const newRow = row + i;
                const newCol = col + j; // Kiểm tra xem ô mới có nằm trong biên không

                if (newRow >= 0 && newRow < gridSize && newCol >= 0 && newCol < gridSize) {
                  neighbors.push(newRow * gridSize + newCol);
                }
              }
            }

            return neighbors;
          };

          this.onResetButtonClicked = async () => {
            // this.stopTime()
            this.startTimer(); // if (this.isRunning) {
            //     this.stopTime();
            // } else {
            //     this.startTimer();
            // }

            await this.mapLayout.removeAllChildren();
            await this.genBox();
          };
        }

        setCurState(value) {
          switch (value) {
            case GameState.GS_INIT:
              this.init();
              break;

            case GameState.GS_PLAYING:
              this.playing();
              break;

            case GameState.GS_END:
              break;
          }
        }

        onPlayButtonClicked() {
          this.setCurState(GameState.GS_PLAYING); // this.startMenu.active = false
          // this.settingMenu.active = false

          this.startTime = (_crd && moment === void 0 ? (_reportPossibleCrUseOfmoment({
            error: Error()
          }), moment) : moment)().unix(); // this.schedule(this.countTime, 1, macro.REPEAT_FOREVER);

          this.startTimer();
        }

        countTime() {
          const currentTime = (_crd && moment === void 0 ? (_reportPossibleCrUseOfmoment({
            error: Error()
          }), moment) : moment)().unix();
          const elapsedSeconds = currentTime - this.startTime;
          const formattedTime = (_crd && moment === void 0 ? (_reportPossibleCrUseOfmoment({
            error: Error()
          }), moment) : moment).utc(elapsedSeconds * 1000).format("HH:mm:ss");
          this.topMenu.getChildByName('Time').getComponent(Label).string = formattedTime;
          console.log(formattedTime);
        }

        startTimer() {
          this.topMenu.getChildByName('Time').getComponent(Label).string = '00:00:00';
          this.isRunning = true;
          this.startTime = (_crd && moment === void 0 ? (_reportPossibleCrUseOfmoment({
            error: Error()
          }), moment) : moment)().unix(); // Gọi hàm updateTimer mỗi giây

          this.schedule(this.countTime, 1, macro.REPEAT_FOREVER);
        }

        stopTime() {
          this.isRunning = false;
          this.unschedule(this.countTime);
        }

        onSettingButtonClicked() {
          this.startMenu.active = false;
          this.settingMenu.active = true;
        }

        onBackButtonClicked() {
          this.startMenu.active = true;
          this.settingMenu.active = false;
        }

        onExitButtonClicked() {
          window.location.href = 'about:blank';
          game.end();
        }

        start() {
          // this.settingMenu.active = false
          this.setCurState(GameState.GS_INIT);
        }

        update(deltaTime) {}

      }, (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "boxPrefab", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "bomPrefab", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "boxIndexPrefab", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "startMenu", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "gameTitle", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "settingMenu", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "topMenu", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "mapLayout", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "clickSound", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=17a5e2f03e686eaab3046e46ad205220bddacfab.js.map