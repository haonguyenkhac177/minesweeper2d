System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, __checkObsolete__, __checkObsoleteInNamespace__, _decorator, Component, Node, game, Prefab, instantiate, Layout, Size, UITransform, Animation, Label, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _crd, ccclass, property, BlockType, GameState, GameManager;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'transform-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      __checkObsolete__ = _cc.__checkObsolete__;
      __checkObsoleteInNamespace__ = _cc.__checkObsoleteInNamespace__;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      game = _cc.game;
      Prefab = _cc.Prefab;
      instantiate = _cc.instantiate;
      Layout = _cc.Layout;
      Size = _cc.Size;
      UITransform = _cc.UITransform;
      Animation = _cc.Animation;
      Label = _cc.Label;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "70dbdkS/wlKAYEwiB4I3JUl", "GameManager", undefined);

      __checkObsolete__(['_decorator', 'Component', 'Node', 'game', 'Prefab', 'instantiate', 'Layout', 'Size', 'UITransform', 'Widget', 'Animation', 'cclegacy', 'Label']);

      ({
        ccclass,
        property
      } = _decorator);

      BlockType = /*#__PURE__*/function (BlockType) {
        BlockType[BlockType["Box1"] = 0] = "Box1";
        BlockType[BlockType["Box2"] = 1] = "Box2";
        return BlockType;
      }(BlockType || {});

      ;

      GameState = /*#__PURE__*/function (GameState) {
        GameState[GameState["GS_INIT"] = 0] = "GS_INIT";
        GameState[GameState["GS_PLAYING"] = 1] = "GS_PLAYING";
        GameState[GameState["GS_END"] = 2] = "GS_END";
        return GameState;
      }(GameState || {});

      ; // export const BLOCK_SIZE = 30;

      _export("GameManager", GameManager = (_dec = ccclass('GameManager'), _dec2 = property({
        type: Prefab
      }), _dec3 = property({
        type: Prefab
      }), _dec4 = property({
        type: Prefab
      }), _dec5 = property({
        type: Node
      }), _dec6 = property({
        type: Node
      }), _dec7 = property({
        type: Node
      }), _dec(_class = (_class2 = class GameManager extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "boxPrefab", _descriptor, this);

          _initializerDefineProperty(this, "bomPrefab", _descriptor2, this);

          _initializerDefineProperty(this, "boxIndexPrefab", _descriptor3, this);

          _initializerDefineProperty(this, "startMenu", _descriptor4, this);

          _initializerDefineProperty(this, "settingMenu", _descriptor5, this);

          _initializerDefineProperty(this, "mapLayout", _descriptor6, this);

          this.minefield = [];

          this.init = async () => {
            await this.genBox();
            console.log(this.mapLayout.children);
            this.mapLayout.children.forEach(i => {
              i.getComponent(Animation).play('showBoxAnimation');
            });
          };

          this.genBox = async () => {
            const newLayout = this.mapLayout.getComponent(UITransform);
            const BLOCK_SIZE = instantiate(this.boxPrefab).getComponent(UITransform).width;
            const sizeBoxNumber = 9;
            const numBombs = 9;
            this.minefield = Array(sizeBoxNumber * sizeBoxNumber).fill(0);

            for (let i = 0; i < numBombs; i++) {
              let bombIndex;

              do {
                // Chọn một ô ngẫu nhiên để đặt bom
                bombIndex = Math.floor(Math.random() * this.minefield.length);
              } while (this.minefield[bombIndex] === -1); // Đặt bom vào ô đã chọn


              this.minefield[bombIndex] = -1; // Tăng giá trị các ô xung quanh bom

              this.increaseNeighbors(bombIndex, sizeBoxNumber);
            }

            console.log('aaa', this.minefield);
            const spacingX = 3;
            const LayoutSize = sizeBoxNumber * (BLOCK_SIZE + spacingX) - spacingX;
            const totalBlock = sizeBoxNumber * sizeBoxNumber;
            this.mapLayout.getComponent(Layout).cellSize = new Size(BLOCK_SIZE, BLOCK_SIZE);
            newLayout.setContentSize(LayoutSize, LayoutSize);

            for (let j = 0; j < totalBlock; j++) {
              switch (this.minefield[j]) {
                case -1:
                  this.mapLayout.addChild(instantiate(this.bomPrefab));
                  break;

                case 0:
                  this.mapLayout.addChild(instantiate(this.boxPrefab));
                  break;

                default:
                  instantiate(this.boxIndexPrefab).getComponentInChildren(Label).string = this.minefield[j].toString();
                  this.mapLayout.addChild(instantiate(this.boxIndexPrefab));
                  break;
              } // if(this.minefield[j]===-1){
              //     this.mapLayout.addChild(instantiate(this.bomPrefab))
              // }else{
              //     this.mapLayout.addChild(instantiate(this.boxPrefab))
              // }

            }
          };

          this.increaseNeighbors = (index, gridSize) => {
            const neighbors = this.getNeighborIndices(index, gridSize);

            for (const neighbor of neighbors) {
              if (this.minefield[neighbor] !== -1) {
                this.minefield[neighbor]++;
              }
            }
          };

          this.getNeighborIndices = (index, gridSize) => {
            const row = Math.floor(index / gridSize);
            const col = index % gridSize;
            const neighbors = [];

            for (let i = -1; i <= 1; i++) {
              for (let j = -1; j <= 1; j++) {
                const newRow = row + i;
                const newCol = col + j; // Kiểm tra xem ô mới có nằm trong biên không

                if (newRow >= 0 && newRow < gridSize && newCol >= 0 && newCol < gridSize) {
                  neighbors.push(newRow * gridSize + newCol);
                }
              }
            }

            return neighbors;
          };
        }

        setCurState(value) {
          switch (value) {
            case GameState.GS_INIT:
              this.init();
              break;

            case GameState.GS_PLAYING:
              break;

            case GameState.GS_END:
              break;
          }
        }

        onPlayButtonClicked() {
          this.startMenu.active = false;
          this.settingMenu.active = false;
        }

        onSettingButtonClicked() {
          this.startMenu.active = false;
          this.settingMenu.active = true;
        }

        onBackButtonClicked() {
          this.startMenu.active = true;
          this.settingMenu.active = false;
        }

        onExitButtonClicked() {
          window.location.href = 'about:blank';
          game.end();
        }

        onResetButtonClicked() {
          console.log('reset');
        }

        start() {
          this.settingMenu.active = false;
          this.setCurState(GameState.GS_INIT);
        }

        update(deltaTime) {}

      }, (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "boxPrefab", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "bomPrefab", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "boxIndexPrefab", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "startMenu", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "settingMenu", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "mapLayout", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=a19142e4d03891bb9cfb83161d3964ac2c3931f0.js.map